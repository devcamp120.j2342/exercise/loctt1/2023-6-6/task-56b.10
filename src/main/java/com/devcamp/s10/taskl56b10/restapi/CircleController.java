package com.devcamp.s10.taskl56b10.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CircleController {
     @GetMapping("/circle-area")
     public double getCircleArea(@RequestParam double radius){
          Circle circle = new Circle(radius);
          return circle.getArea();
     }
}
