package com.devcamp.s10.taskl56b10.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CylinderController {
     @GetMapping("/cylinder-volume")
     public double getCylinderVolume(@RequestParam double radius,@RequestParam double height){
          Cylinder cylinder = new Cylinder(radius, height);
          return cylinder.getVolume();
     }
}
